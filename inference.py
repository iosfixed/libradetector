from pathlib import Path

import numpy as np
import pandas as pd
import torch
from torch.utils.data import DataLoader

from misc.dataset import load_series
from data_stuff.timeseries_transforms import ResampleSeriesTransform, SinCosWrapper


class InferenceEngine():
    def __init__(self, model_path, resample_points=None, use_cuda=False):
        model = torch.load(model_path)
        device = torch.device('cuda' if use_cuda else 'cpu')

        model.to(device)
        model.eval()

        self.model = model
        self.device = device
        self.resampler = ResampleSeriesTransform(resample_points)

    def __call__(self, series_list, batch_size=256):
        series_list = [self.resampler(series) for series in series_list]
        series_list = [SinCosWrapper.wrap(series) for series in series_list]

        loader = DataLoader(series_list, batch_size=batch_size)
        pred_list = []
        for batch in loader:
            batch = batch.to(self.device)

            with torch.no_grad():
                outputs = self.model(batch)
                predictions = torch.relu(outputs)
                pred_list.append(predictions.cpu().numpy())

        return np.concatenate(pred_list)


def inference(config):
    # TODO: Use last model in model_path isn't specified
    if 'model_path' not in config.as_dict():
        # pt_pathes = filter(lambda f: f.parent, TRAINED_MODELS_DIR.glob('**/*.pt'))
        # sorted_logdirs = sorted(pt_pathes, key=lambda d: d.stat().st_ctime)

        raise Exception('Missing "model_path" key in config.')

    if 'series_path' not in config.as_dict():
        raise Exception('Missing "series_path" key in config.')

    model_path = Path(config['model_path'])
    series_path = Path(config['series_path'])

    if not model_path.exists():
        raise Exception('Model doesn\'t exists. Check path to pretrained model.')

    if not series_path.exists():
        raise Exception('Series directory doesn\'t exists. Check path to series.')

    engine = InferenceEngine(model_path, config['resample_points'], use_cuda=True)

    series_pathes = list(series_path.glob('**/*.npy')) + \
                    list(series_path.glob('**/*.txt')) + \
                    list(series_path.glob('**/*.csv'))

    series_pathes = [f for f in series_pathes if not f.name.startswith('report')]  # Exclude reports.
    if not series_pathes:
        raise Exception('Series directory doesn\'t contains any suitable series file. '
                        'Check inference section in readme')

    series_list = [load_series(path) for path in series_pathes]

    pred = engine(series_list)

    dataframe = pd.DataFrame()
    dataframe['src'] = series_pathes
    dataframe['pred'] = pred
    dataframe['pred_class'] = (pred > 0.5).astype(int)

    model_name = model_path.name.split('.')[0]
    df_fname = f'report_{model_name}.csv'
    dataframe.to_csv(series_path / df_fname, index=False)

    return str(series_path / df_fname)

