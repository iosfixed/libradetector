import json
from os import makedirs

import numpy as np
import torch
from torch import nn
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

from consts import *
from data_stuff.timeseries_transforms import RandomResampleSeriesTransform
from misc.utils import is_jsonable, init_dataloaders, init_model, Metrics


def train_classifier(config):
    # Init dataloaders
    dataloaders = init_dataloaders(config)
    config['classes_map'] = class_names_map

    # Initialize model
    model, optimizer, scheduler = init_model(config)
    classifier_loss_fn = nn.modules.loss.SmoothL1Loss()

    # Initialize loggers
    writer = SummaryWriter(config['log_dir'])
    metrics = Metrics(writer=writer)

    device = torch.device(config['device'])

    # TODO: Replace with config.save
    with open(Path(config['model_dir']) / f'experiment_config_{config["mode"]}.json', 'w') as f:
        serializable_config = {key: value for key, value in config.as_dict().items() if is_jsonable(value)}
        json.dump(serializable_config, f, indent=2)

    progress_bar = tqdm(range(config['epochs']))
    for epoch in progress_bar:
        for phase in ['train', 'val']:
            model.training = phase == 'train'

            running_loss, running_ans, running_pred = 0.0, [], []

            batches = len(dataloaders[phase])
            for batch_number, batch in enumerate(dataloaders[phase]):
                if dataloaders[phase].dataset.return_src:
                    *batch, pathes = batch

                inputs, labels = batch

                # Changing resampling points for next batch:
                for aug in dataloaders[phase].dataset.aug:
                    if isinstance(aug, RandomResampleSeriesTransform):
                        aug.refresh()
                        break

                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs.to(device))

                    float_labels = labels.unsqueeze(1).float()
                    float_labels = float_labels.to(device)

                    loss = classifier_loss_fn(outputs, float_labels)

                    # weights = torch.ones_like(loss)
                    # weights[float_labels == 1] = 3

                    # loss = loss * weights
                    # loss = loss.mean()

                    probs = outputs

                    if phase == 'train':
                        optimizer.zero_grad()
                        loss.backward()
                        optimizer.step()

                running_loss += loss.item() * inputs.size(0)
                running_ans.append(labels.cpu().numpy().flatten())
                running_pred.append(probs.detach().cpu().numpy().flatten())

                desc = metrics.desc(epoch, phase, batch_number, batches)
                progress_bar.set_description(desc=desc)

            # End of phase
            if phase == 'train':
                scheduler.step()

            answers = np.concatenate(running_ans)
            predictions = np.concatenate(running_pred)

            metrics.update(answers, predictions, running_loss, phase)
            metrics.tensorboard_write(phase=phase, epoch=epoch)

            if metrics.loss_dict['val'] == metrics.loss_dict['best']:
                torch.save(model, config['model_dir'] / ENTIRE_MODEL_FILENAME)

        # End of epoch
        metrics.tensorboard_write(name='lr', value=scheduler.get_lr()[0], epoch=epoch)

        it_is_time_to_save_model = (epoch % config['checkpoint_epochs'] == 0)
        it_is_last_epoch = (epoch == config['epochs'] - 1)

        if it_is_time_to_save_model or it_is_last_epoch:
            torch.save(model, config['model_dir'] / f'{epoch + 1}_epoch_model.pt')

