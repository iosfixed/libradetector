import time
from functools import partial
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Border colors for toggle_border()
bad_pic_spine_color_tuple = (1, 0, 0, 1)
good_pic_spine_color_tuple = (0, 0, 0, 0)

# Specify your local paths!
series_root = Path('/home/dmitry/DATA/libradetector_datasets/small_val/train/libration')
marker_df_path = series_root / 'marker_df.csv'

def key_to_idx(key):
    if key < 4:
        idx = 2 + key
    else:
        idx = key - 4

    return idx


def toggle_border(ax):
    for spine in ax.spines.values():
        if spine._edgecolor != bad_pic_spine_color_tuple:
            spine.set_edgecolor(bad_pic_spine_color_tuple)
            spine.set_linewidth(5)
        else:
            spine.set_edgecolor(good_pic_spine_color_tuple)


def ETA(state):
    if not state.get('time_per_batches'):
        state['time_per_batches'] = []

        return ''

    mean_time_per_batch = np.mean(state['time_per_batches'])

    estimated_seconds = (state['total_batches'] - state['batch']) * mean_time_per_batch
    MM = int(estimated_seconds / 60 % 60)
    HH = int(estimated_seconds / 60 // 60)

    ETA_string = f"Batch {state['batch']} / {state['total_batches']}"
    ETA_string += f'Elapsed time {HH:02d}:{MM:02d} ({mean_time_per_batch:.1f} sec per batch'

    return ETA_string


def stopwatch(state):
    tic = state.get('tic', time.time())
    toc = time.time()

    state['time_per_batches'].append(toc - tic)
    state['tic'] = toc

    return


def show_photos(state):
    batch = state['batch']
    batch_start, batch_end = state['batches_borders'][batch:batch + 2]

    state['batch_pic_paths'] = state['pic_paths'][batch_start:batch_end]

    for ax, pic_path in zip(state['axes'], state['batch_pic_paths']):
        pic = np.load(pic_path).ravel()
        ax.scatter(range(pic.size), pic, s=3)
        ax.set_ylim(0, 2*np.pi)
        pic_name = pic_path.name.split('.')[0]

        ax.set_title(f'{pic_name}')

        for spine in ax.spines.values():
            spine.set_edgecolor((0, 0, 0, 0))

    eta = ETA(state)
    plt.suptitle(eta)

    fig.canvas.draw()
    return


def press(event, state):
    if event.key in list('123456'):
        key = int(event.key)
        idx = key_to_idx(key)
        ax = axes[idx]

        if idx not in state['bad_pics_idx']:
            state['bad_pics_idx'].append(idx)
        else:
            state['bad_pics_idx'].pop(state['bad_pics_idx'].index(idx))

        toggle_border(ax)
        fig.canvas.draw()

    if event.key in list(' 0'):
        stopwatch(state)

        for ax in state['axes']:
            ax.clear()

        for idx, img in enumerate(state['batch_pic_paths']):
            img_name = Path(img).name.split('.')[0]
            is_suitable = int(idx not in state['bad_pics_idx'])
            new_marker_df_line = dict(img=img_name, is_suitable=is_suitable)
            state['marker_df'] = state['marker_df'].append(new_marker_df_line, ignore_index=True)

        state['marker_df'].to_csv(marker_df_path, index=False)

        state['bad_pics_idx'].clear()
        state['batch'] += 1

        show_photos(state)

    if event.key == 'q':
        exit(0)

    return


if marker_df_path.exists():
    marker_df = pd.read_csv(marker_df_path)
else:
    marker_df = pd.DataFrame(columns=['img', 'is_suitable'])

processed_pics = set(marker_df.img.values)

pics_glob = series_root.glob(f'*.npy')
pic_paths = [pic for pic in pics_glob if pic.name.split('.')[0] not in processed_pics]
pic_paths = sorted(pic_paths)

batches_borders = list(range(0, len(pic_paths), 6)) + [len(pic_paths) - 1]

fig, axes = plt.subplots(2, 3, figsize=(20, 15))
axes = axes.ravel()

state = dict(
    marker_df=marker_df,
    batches_borders=batches_borders,
    total_batches=len(batches_borders) - 1,
    pic_paths=pic_paths,
    axes=axes,
    bad_pics_idx=[],
    batch=0,
)

fig.canvas.mpl_connect('key_press_event', partial(press, state=state))
show_photos(state)
plt.show()
