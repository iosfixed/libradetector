from os.path import expanduser
from pathlib import Path
import numpy as np

RANDOM_SEED = 42

CONFIGS_DIR = Path('./configs')

TRAINED_MODELS_DIR = Path(expanduser('~/DATA/logs/libradetector/'))
LOGS_ROOT = Path(expanduser('~/DATA/logs/libradetector/'))
DATASETS_BASE_DIR = Path(expanduser('~/DATA/libradetector_datasets/'))

ENTIRE_MODEL_FILENAME = 'entire_best_model.pt'
STATE_DICT_FILENAME = 'state_dict_best_model.pt'
REC_PLOT_STEPS = 5
IMG_SIZE = 299

classes_idx_map = {'circulation': 0,
                   'libration': 1,
                   'mixed': 0.5}

class_names_map = {value: key for key, value in classes_idx_map.items()}