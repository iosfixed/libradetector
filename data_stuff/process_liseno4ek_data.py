from os import makedirs

import numpy as np
from uuid import uuid4
from pathlib import Path

rad = np.pi / 180
data_dir = Path('/home/dmitry/Science/liseno4ek')
dst_root = Path('/home/dmitry/DATA/resonances_classifier/src')

data_fname = lambda N: 'crit_M_%04d.dat' % (N+1)
class_map = {1: 'libration', 2: 'mixed_libration', 3: 'mixed_circulation', 4: 'circulation'}

class_estimates = {}

class_estimates['16_24'] = [(1, 574, 4),
                            (575, 576, 2),
                            (577, 588, 1),
                            (589, 798, 4)]

class_estimates['24_33'] = [(1, 522, 4),
                            (523, 526, 2),
                            (527, 546, 4),
                            (547, 588, 1),
                            (589, 596, 4),
                            (597, 601, 4),
                            (602, 630, 1),
                            (631, 662, 4),
                            (663, 672, 1),
                            (673, 706, 4),
                            (707, 714, 2),
                            (715, 750, 4),
                            (751, 756, 2),
                            (757, 798, 4)]

class_estimates['33_41'] = [(1, 428, 4),
                            (429, 436, 2),
                            (437, 482, 4),
                            (483, 546, 2),
                            (547, 714, 1),
                            (715, 721, 2),
                            (722, 756, 1),
                            (757, 765, 2),
                            (766, 798, 1)]

class_estimates['41_45'] = [(1, 187, 4),
                            (188, 323, 1)]

for key, tuples in class_estimates.items():
    estimates_list = np.zeros(tuples[-1][1], dtype=np.int)
    for tup in tuples:
        first, last, estimate = tup
        estimates_list[first-1:last] = estimate

    class_estimates[key] = estimates_list


target_folders = [path for path in data_dir.glob('*') if path.name in class_estimates.keys()]

for folder in target_folders:
    estimates = class_estimates[folder.name]
    for N, estimate in enumerate(estimates):
        args = np.loadtxt(folder / data_fname(N))
        kozai = args[:, -1] * rad

        fname = f'{uuid4()}.npy'
        dst_dir = dst_root / class_map[estimate]
        makedirs(dst_dir, exist_ok=True)
        np.save(dst_dir / fname, kozai)
