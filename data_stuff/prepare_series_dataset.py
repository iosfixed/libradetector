import random
from functools import partial
from multiprocessing.pool import Pool
from os import makedirs
from pathlib import Path
from uuid import uuid4

import numpy as np
from tqdm import tqdm

from consts import class_names_map, classes_idx_map
from misc.dataset import TimeSeriesDataset
from data_stuff.timeseries_transforms import PhaseShiftTransform, CutTransform, NormalizeAngle

val_split = 0.8
samples_per_class = 1000
tranfroms_list = [PhaseShiftTransform(),
                  CutTransform(),
                  NormalizeAngle()
                  # ReverseTransform(),
                  # lambda x: x / 2 / np.pi
                  ]

base_dir = Path('/home/dmitry/DATA/resonances_classifier/src_2_classes')
dst_root = Path('/home/dmitry/DATA/resonances_classifier/datasets')


def create_and_save_series(idx, class_name, dataset, dataset_name, val_split):
    series, _ = dataset[idx]

    fold = 'val' if np.random.rand() < val_split else 'train'
    fname = f'{uuid4()}.npy'

    dst = dst_root / dataset_name / fold / class_name / fname
    np.save(dst, series)

    return None


dataset_names = ['series_dataset', 'series_dataset_test']
for dataset_name in dataset_names:
    for set_name in ['train', 'val']:
        for class_name in classes_idx_map:
            makedirs(dst_root / dataset_name / set_name / class_name, exist_ok=True)

datafiles = list(base_dir.glob('*/*.npy'))
random.shuffle(datafiles)
test_split_threshold = int(len(datafiles) * 0.8)
train_src, test_src = datafiles[:test_split_threshold], datafiles[test_split_threshold:]

srcs = train_src, test_src
for src, name in zip(srcs, dataset_names):
    dataset = TimeSeriesDataset(src, aug=tranfroms_list)

    class_table = {key: [] for key in set(dataset.answers)}
    for idx, ans in enumerate(dataset.answers):
        class_table[ans].append(idx)

    for class_idx, sample_indices in class_table.items():
        class_name = class_names_map[class_idx]
        random_samples_from_class = np.random.choice(sample_indices, samples_per_class, replace=True)
        processing_function = partial(create_and_save_series, dataset=dataset, class_name=class_name,
                                      dataset_name=name, val_split=val_split)

        with tqdm(desc=f'Processing {class_name}... Total {len(sample_indices)} src files') as pbar:
            with Pool(6) as p:
                for _ in p.imap(processing_function, random_samples_from_class):
                    pbar.update()


