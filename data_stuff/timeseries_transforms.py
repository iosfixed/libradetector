import numpy as np
from scipy.interpolate import interp1d


# Transformations for data_stuff augmentation

class PhaseShiftTransform(object):
    """
    Shift series for some constant angle.
    """

    def __call__(self, series):
        phi = 2 * np.pi * np.random.rand()
        series = series + phi

        return series


class ReverseTransform(object):
    """
    Reverse time series
    """

    def __call__(self, series):
        if np.random.rand() > 0.5:
            return series[::-1]
        return series


class CutTransform(object):
    """
    Cut series
    """
    k = 0.2  # See docstring

    def __call__(self, series):
        rand = np.random.randint(1, self.k * series.size, size=2)
        new_series = series[..., rand[0]:-rand[1]]

        return new_series

class NormalizeAngle:
    def __init__(self):
        self.period = 2 * np.pi

    def __call__(self, x):
        return x - self.period * (x // self.period)

class NormalizeSeriesTransform(object):
    """
    Reverse time series
    """

    def __call__(self, series):
        return series / 2 / np.pi


class ResampleSeriesTransform(object):
    """
    Reverse time series
    """

    def __init__(self, samples=None):
        self.samples = samples

    def __call__(self, series):
        if self.samples is None:
            return series

        f = interp1d(np.arange(series.size), series)

        x_grid = np.linspace(0, series.size - 1, self.samples, endpoint=False)

        return f(x_grid)

class RandomResampleSeriesTransform(object):
    """
    Reverse time series
    """

    def __init__(self, low=1000, high=3000):
        self.low = low
        self.high = high

        self.refresh()

    def refresh(self):
        self.samples = np.random.randint(self.low, self.high)

    def __call__(self, series):
        if self.samples is None:
            return series

        f = interp1d(np.arange(series.size), series)

        x_grid = np.linspace(0, series.size - 1, self.samples, endpoint=False)

        return f(x_grid)


class RNNWrapper(object):
    """
    Wraps series to RNN friendly format
    """

    def __init__(self):
        pass

    @staticmethod
    def wrap(series):
        series

        return series

    def __call__(self, series):
        return self.wrap(series)


class SinCosWrapper(object):
    """
    Wraps angle series to sin and cos for the sake of continuity
    """

    def __init__(self):
        pass

    @staticmethod
    def wrap(series):
        series = np.vstack([np.sin(series), np.cos(series)])
        series = 0.5 + 0.5 * series  # Normalize to 0..1
        series = series.astype('float32')

        return series

    def __call__(self, series):
        return self.wrap(series)


class NoiseTransform(object):
    """
    Reverse time series
    """

    def __init__(self, sigma_degrees=1):
        self.sigma = sigma_degrees * np.pi / 180  # in degrees

    def __call__(self, series):
        return series + self.sigma * np.random.randn(*series.shape)


class Deg2RadTransform(object):
    """
    Reverse time series
    """

    def __call__(self, series):
        return series * np.pi / 180
