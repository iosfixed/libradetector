import os
from datetime import datetime
from pathlib import Path
from researchutils.cfg import with_config

from consts import LOGS_ROOT, DATASETS_BASE_DIR
from inference import inference
from train import train_classifier


@with_config('configs/train/default')
def main(config):
    cfg = config
    if cfg['mode'] == 'inference':
        path_to_result_df = inference(cfg)
        print(f'Inference finsihed successfully. See results in {path_to_result_df}')
        exit()

    # Get current time to make timestamp
    timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
    cfg['timestamp'] = timestamp

    # If model directory isn't specified - create new
    if 'model_dir' not in cfg.as_dict():
        cfg['model_dir'] = LOGS_ROOT / f'{timestamp}_{cfg["name"]}'
        cfg['log_dir'] = cfg['model_dir']
        os.makedirs(cfg['model_dir'], exist_ok=True)
    else:
        cfg['model_dir'] = Path(cfg['model_dir'])
        cfg['log_dir'] = Path(cfg['model_dir'])

    # Check dataset directory
    if not (DATASETS_BASE_DIR / cfg['dataset']).exists():
        raise Exception(f'Couldn`t find dataset. ({str(DATASETS_BASE_DIR / cfg["dataset"])})')

    # set_random_seed(RANDOM_SEED)

    # Dump config to model dir
    cfg_path = Path(cfg['model_dir']) / f'experiment_config_{cfg["mode"]}.json'
    config.save_json_config(cfg_path)

    # TODO: Add autoencoder problem
    if cfg['mode'] == 'train':
        problem = cfg.as_dict().get('problem', 'classification')
        if problem == 'classification':
            train_classifier(cfg)
        else:
            raise Exception(f'Unknown problem {problem}')



if __name__ == '__main__':
    main()
