from pathlib import Path

import numpy as np
from torch.utils.data import Dataset

from consts import classes_idx_map


def load_series(src):
    if not isinstance(src, Path):
        src = Path(src)

    if src.name.endswith('.npy'):
        series = np.load(src)

    elif src.name.endswith('.txt'):
        try:
            series = np.loadtxt(src)
        except:
            raise Exception(f'Source file {src.name} has invalid data format. '
                            f'See inference section in readme.')

    elif src.name.endswith('.csv'):
        try:
            series = np.loadtxt(src, delimiter=',')
        except:
            raise Exception(f'Source file {src.name} has invalid data format. '
                            f'See inference section in readme.')

    return series.reshape(1, -1)


class TimeSeriesDataset(Dataset):
    def __init__(self, datafiles, aug=None, preproc=None, return_src=False):
        self.return_src = return_src

        self.sources = sorted(list(datafiles))
        self.answers = [classes_idx_map.get(file.parent.name, -1) for file in self.sources]

        negative_count = self.answers.count(0)
        positive_count = self.answers.count(1)

        if positive_count and negative_count:
            self.pos_weight = negative_count / positive_count

        self.len = len(self.sources)
        self.aug = aug
        self.preproc = preproc

    def __len__(self):
        return self.len

    def __getitem__(self, item):
        series = load_series(self.sources[item])
        if self.aug is not None:
            for aug in self.aug:
                series = aug(series)

        if len(series.shape) == 1:
            series = series.reshape(1, -1)

        src = self.sources[item]

        if self.return_src:
            return series, self.answers[item], str(src)

        return series, self.answers[item]
