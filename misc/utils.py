import json
import random
from argparse import ArgumentParser

import torch
from sklearn.metrics import roc_auc_score
from torch import nn
from torch import optim
from torch.utils.data import DataLoader
from torch.utils.data._utils.collate import default_collate

from consts import CONFIGS_DIR, DATASETS_BASE_DIR
from misc.dataset import TimeSeriesDataset
from data_stuff.timeseries_transforms import *
from models.model_keeper import get_model
from misc.radam import RAdam


class Metrics:
    def __init__(self, writer):
        self.writer = writer

        self.acc_dict = dict(best=0., train=0., val=0.)
        self.roc_dict = dict(best=0., train=0., val=0.)
        self.loss_dict = dict(best=float('+inf'), train=float('+inf'), val=float('+inf'))
        self.update_strings()

    def update_strings(self):
        self.acc_str = "Accuracy: ({train:1.5} / {val:1.5} / {best:1.5})".format(**self.acc_dict)
        self.roc_str = "ROC AUC: ({train:1.5} / {val:1.5} / {best:1.5})".format(**self.roc_dict)
        self.loss_str = "Loss: ({train:1.5} / {val:1.5} / {best:1.5})".format(**self.loss_dict)

    def update(self, answers, predictions, running_loss, phase):
        self.loss_dict[phase] = running_loss / len(answers)
        self.acc_dict[phase] = (answers == (predictions > 0.5).astype(int)).mean()
        self.roc_dict[phase] = roc_auc_score(answers, predictions)

        self.roc_dict['best'] = max(self.roc_dict['best'], self.roc_dict['val'])
        self.acc_dict['best'] = max(self.acc_dict['best'], self.acc_dict['val'])
        self.loss_dict['best'] = min(self.loss_dict['best'], self.loss_dict['val'])

        self.update_strings()

    def desc(self, epoch, phase, batch_number, batches):
        desc = '\t'.join((f'Epoch {epoch} {phase}.', f'Batch {batch_number + 1}/{batches}',
                          self.acc_str, self.loss_str, self.roc_str))

        return desc

    def tensorboard_write(self, **kwargs):
        epoch = kwargs['epoch']

        if {'name', 'value', 'epoch'} == set(kwargs):
            self.writer.add_scalar(kwargs['name'], kwargs['value'], epoch)

        else:
            phase = kwargs['phase']
            self.writer.add_scalar(phase + ' loss', self.loss_dict[phase], epoch)
            self.writer.add_scalar(phase + ' acc', self.acc_dict[phase], epoch)
            self.writer.add_scalar(phase + ' ROC AUC', self.roc_dict[phase], epoch)

        self.writer.flush()


def is_jsonable(x):
    try:
        json.dumps(x)
        return True
    except:
        return False


def set_random_seed(seed):
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)


def load_config():
    parser = ArgumentParser()

    parser.add_argument('--config', type=str, required=True)
    parser.add_argument('--name', type=str, required=False)
    parser.add_argument('--dataset', type=str, required=False)
    parser.add_argument('--model_dir', type=str, required=False)

    args = parser.parse_args()
    args_dict = {key: item for key, item in vars(args).items() if item is not None}

    cfg_path = CONFIGS_DIR / f'{args_dict["config"]}.json'
    with open(cfg_path, 'r') as f:
        config = json.load(f)

    mode = config['mode']
    default_cfg_path = CONFIGS_DIR / f'default_{mode}.json'
    with open(default_cfg_path, 'r') as f:
        default_config = json.load(f)

    default_config.update(config)  # Overwrite default_config values by config values
    default_config.update(args_dict)  # Overwrite overwritten values by command line arguments

    return default_config


def init_model(cfg):
    model = get_model(cfg)
    model.to(cfg['device'])

    # optimizer = torch.optim.Adam(model.parameters(), lr=cfg['lr'], weight_decay=cfg["decay"])
    optimizer = RAdam(model.parameters(), lr=cfg['lr'], weight_decay=cfg["decay"])
    # optimizer = torch.optim.SGD(model.parameters(), lr=cfg['lr'], nesterov=True, momentum=0.8)
    gamma = cfg['scheduler_gamma']

    scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=gamma)

    return model, optimizer, scheduler


def init_dataloaders(config, folds=None):
    augs = [CutTransform(),
            ReverseTransform(),
            PhaseShiftTransform(),
            NoiseTransform()]

    # Using random resampler if resampling points doesn't specified in config
    if config.resample_points:
        resampler = ResampleSeriesTransform(config['resample_points'])
    else:
        resampler = RandomResampleSeriesTransform()

    preproc = [
        resampler,
        SinCosWrapper()]

    transforms = {
        'train': augs + preproc,
        'val': augs + preproc,
        'test': preproc,
    }

    if folds is None:
        folds = [folder.name for folder in (DATASETS_BASE_DIR / config['dataset']).glob('*')]

    return_src = True if config['mode'] == 'test' else False

    src = {fold: (DATASETS_BASE_DIR / config['dataset'] / fold).glob('**/*.npy') for fold in folds}
    series_datasets = {fold: TimeSeriesDataset(src[fold], transforms[fold], return_src=return_src) for fold in folds}

    dataloaders = {fold: DataLoader(series_datasets[fold],
                                    batch_size=config['batch_size'],
                                    shuffle=(fold == 'train'),
                                    drop_last=False,
                                    num_workers=config['dataloader_workers']) for fold in folds}

    return dataloaders
