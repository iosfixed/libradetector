import torch
from torch import nn
from torch.utils.data import Dataset
# from torch_ard import LinearARD


class NextGenConv(nn.Module):
    def __init__(self, config):
        super(NextGenConv, self).__init__()

        c1_ch = 20
        c2_ch = 50
        c3_ch = 100
        c4_ch = 200

        self.features = nn.Sequential(
            nn.Conv1d(2, c1_ch, 100, stride=10),
            nn.ReLU(),
            nn.Conv1d(c1_ch, c2_ch, 20),
            nn.ReLU(),
            nn.MaxPool1d(4),
            nn.BatchNorm1d(c2_ch),

            nn.Conv1d(c2_ch, c3_ch, 10),
            nn.ReLU(),
            nn.Conv1d(c3_ch, c4_ch, 5),
            nn.ReLU(),
            nn.MaxPool1d(2),
            nn.BatchNorm1d(c4_ch),
        )

        self.adapool = nn.AdaptiveAvgPool1d(1)

        self.dropout1 = nn.Dropout(config.dropout_rate)
        self.dropout2 = nn.Dropout(config.dropout_rate)

        if config.use_ard:
            self.fc1 = LinearARD(200, 10)
            self.fc2 = LinearARD(10, 2)
        else:
            self.fc1 = nn.Linear(200, 10)
            self.fc2 = nn.Linear(10, 2)

    def forward(self, x):
        x = self.features(x)
        x = self.adapool(x)

        x = x.view(x.size(0), -1)

        x = self.dropout1(x)
        x = self.fc1(x)

        x = self.dropout2(x)
        x = self.fc2(x)

        return x
