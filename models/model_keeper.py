from models.conv1d import ConvModel
from models.gru_model import GRUModel
from models.gru_state_model import GRUStateModel
from models.lite_conv import LiteConv
from models.next_get_conv import NextGenConv

models = {
    'default': ConvModel,
    'conv': ConvModel,
    'lite': LiteConv,
    'next_gen': NextGenConv,
    'nextgen': NextGenConv,
    'gru': GRUModel,
    'state_gru': GRUStateModel
}

def get_model(cfg):
    model_name = cfg["model"]
    try:
        return models[model_name](cfg)
    except:
        raise Exception(f'Unknown model {model_name}')