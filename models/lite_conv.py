import torch
from torch import nn
from torch.utils.data import Dataset

class LiteConv(nn.Module):
    def __init__(self, config):
        super(LiteConv, self).__init__()
        self.relu = nn.ReLU()

        self.conv1 = nn.Conv1d(2, 10, 11)
        self.conv2 = nn.Conv1d(10, 20, 9)
        self.pool1 = nn.MaxPool1d(2)
        self.bn1 = nn.BatchNorm1d(20)

        self.conv3 = nn.Conv1d(20, 50, 5)
        self.conv4 = nn.Conv1d(50, 100, 3)
        self.bn2 = nn.BatchNorm1d(100)

        self.adapool = nn.AdaptiveAvgPool1d(1)
        self.drop = nn.Dropout(config.dropout_rate)

        self.fc1 = nn.Linear(100, 10)
        self.fc2 = nn.Linear(10, 1)

    def forward(self, x):
        x = self.conv1(x)
        x = self.relu(x)

        x = self.conv2(x)
        x = self.relu(x)

        x = self.pool1(x)
        x = self.bn1(x)

        x = self.conv3(x)
        x = self.relu(x)

        x = self.conv4(x)
        x = self.relu(x)
        x = self.bn2(x)

        x = self.adapool(x)
        x = x.view(x.size(0), -1)
        x = self.drop(x)

        x = self.fc1(x)
        x = self.fc2(x)

        return x


