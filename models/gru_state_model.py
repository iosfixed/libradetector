import torch
from torch import nn
from torch.utils.data import Dataset

class GRUStateModel(nn.Module):
    def __init__(self, config):
        super(GRUStateModel, self).__init__()

        self.gru1_layers = 2
        self.gru2_layers = 1

        self.gru1_hidden_size = 128
        self.gru2_hidden_size = 16

        self.relu = nn.ReLU()

        self.gru1 = nn.GRU(2, self.gru1_hidden_size, self.gru1_layers, batch_first=True)
        # self.gru2 = nn.GRU(self.gru1_hidden_size, self.gru2_hidden_size, self.gru2_layers, batch_first=True)

        self.bn1 = nn.BatchNorm1d(self.gru1_layers * self.gru1_hidden_size)
        # self.bn2 = nn.BatchNorm1d(self.gru2_layers * self.gru2_hidden_size)

        self.dropout = nn.Dropout(config.dropout_rate)

        self.fc1 = nn.Linear(self.gru1_layers * self.gru1_hidden_size, 3)
        self.fc2 = nn.Linear(3, 1)

        self.activation = nn.Sigmoid()

    def forward(self, x):
        x = x.transpose(1, 2)  # Do channels last

        x, state = self.gru1(x)
        # x, state = self.gru2(x)

        state = state.transpose(0, 1)  # Do batch_first.
        x = state.reshape(state.size(0), -1)

        x = self.relu(x)
        x = self.bn1(x)

        x = self.fc1(x)
        # x = self.dropout(x)

        x = self.fc2(x)

        x = self.activation(x)

        return x


