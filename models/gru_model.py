import torch
from torch import nn
from torch.utils.data import Dataset

class GRUModel(nn.Module):
    def __init__(self, config):
        super(GRUModel, self).__init__()
        self.resample_points = config.resample_points
        self.relu = nn.ReLU()

        self.gru1 = nn.GRU(self.resample_points, 32, 4, batch_first=True)
        self.gru2 = nn.GRU(32, 64, 4, batch_first=True)

        self.bn1 = nn.BatchNorm1d(2)
        self.dropout = nn.Dropout(config.dropout_rate)
        fc_input = self.gru2.hidden_size * 2
        self.fc = nn.Linear(fc_input, 1)

    def forward(self, x):
        x, _ = self.gru1(x)
        x = self.relu(x)
        x = self.bn1(x)

        x, _ = self.gru2(x)
        x = self.relu(x)
        x = self.dropout(x)

        x = x.reshape(x.size(0), -1)
        x = self.fc(x)

        return x


