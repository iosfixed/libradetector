import torch
from torch import nn

class ConvModel(nn.Module):
    def __init__(self, config):
        super(ConvModel, self).__init__()
        self.relu = nn.ReLU()

        self.conv1 = nn.Conv1d(2, 20, 25)
        self.conv2 = nn.Conv1d(20, 40, 15)
        self.pool1 = nn.MaxPool1d(2)
        self.bn1 = nn.BatchNorm1d(40)

        self.conv3 = nn.Conv1d(40, 100, 9)
        self.conv4 = nn.Conv1d(100, 200, 9)
        self.pool2 = nn.MaxPool1d(2)
        self.bn2 = nn.BatchNorm1d(200)

        self.conv5 = nn.Conv1d(200, 400, 5)
        self.conv6 = nn.Conv1d(400, 500, 5)
        self.pool3 = nn.MaxPool1d(2)

        self.bn3 = nn.BatchNorm1d(500)

        self.adapool = nn.AdaptiveAvgPool1d(1)

        self.fc = nn.Linear(500, 2)

    def forward(self, x):
        x = self.conv1(x)
        x = self.relu(x)

        x = self.conv2(x)
        x = self.relu(x)

        x = self.pool1(x)
        x = self.bn1(x)

        x = self.conv3(x)
        x = self.relu(x)

        x = self.conv4(x)
        x = self.relu(x)

        x = self.pool2(x)
        x = self.bn2(x)

        x = self.conv5(x)
        x = self.relu(x)

        x = self.conv6(x)
        x = self.relu(x)

        x = self.pool3(x)
        x = self.bn3(x)

        x = self.adapool(x)
        x = self.fc(x.view(x.size(0), -1))

        return x


